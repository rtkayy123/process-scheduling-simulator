/*


    Program illustrating a FCFS Scheduling algorithm visually.
    Started on Mon. 7th of October 2019.
    Last Modified on Sun. 13th of October 2019.
    

    The program reads a file passed as an argument - this file should have 3 columns, processName, arrival time, service time;
    Program reads these values and makes a visual representation of their execution and waiting states;

*/
#include "process-visualiser.h"
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include  <unistd.h>
#define LSIZ 128  //stores the line size of each row
void passFile(char *arr); //method takes file as argument, if none is entered, use default. Chart is drawn from values in this file.

 /* Special enumerated data type for process state */
typedef enum {
     READY, RUNNING, EXIT
 } process_state_t;
/*C data structure used as process control block. The scheduler should create
one instance per running process in the system  */
typedef struct {
  char process_name[11]; //a string that defines the process
  /* Times measured in seconds...*/
  int entryTime;    //time process entered system
  int serviceTime;  //total CPU time required by the process
  int remainingTime;  //remaining service time until completion
  int finishingTime; //finishing time on the timeline for that process
  int waitTime;       //entryTime minus when burst time starts on the timeline

  process_state_t state; //current process state (e.g READY)
 } pcb_t;
 
int main(int argc, char **argv){
  initInterface("silver","blue");
  if(argc <2){
    passFile("process-data.txt");
  }
  if(argc == 2){
    passFile(argv[1]);
  } else {
      printf("Invalid arguments");
      exit(0);
  }
  return 0;
}

void passFile(char *arr){
   char line[LSIZ];
   char str[11];
   char str1[11];
   int arrivalTime;
   int serviceTime;
   int prevProcessEndTime = 0;
   int waitingTime = 0;
   int i = 0;
   FILE *fptr = NULL;
   int processCount = -1;
   int finishTime;
   int finishTImecur =0;
   int waitLength = 0;
   fptr = fopen(arr,"r");
   char c;
   for(c = getc(fptr);c!=EOF;c=getc(fptr)){
    if(c=='\n'){
      processCount = processCount+1; //get the number of lines in the file = no. of processes
    }
   }
   pcb_t current_process[processCount]; //store all the processes in an array of the type struct
   if ( NULL != ( fptr = fopen(arr, "r"))) { //open the file in read only mode
        while ( fgets ( line, LSIZ, fptr)){ //record each line
            if ( 3 == sscanf ( line, "%10s %d %d", str1, &arrivalTime, &serviceTime)) { //get each row column by column and store it in respective variables
                process_state_t current_state = READY;
                strcpy(current_process[i].process_name, str1);
                current_process[i].entryTime = arrivalTime;
                current_process[i].serviceTime = serviceTime;
                current_process[i].remainingTime = waitingTime+serviceTime;
                current_process[i].state = READY;
                i = i+1; //increment i by 1 to access next item's data like how long it will wait.
                finishTime = serviceTime + prevProcessEndTime; 
                prevProcessEndTime = finishTime;
                current_process[i].finishingTime = prevProcessEndTime;
                waitLength = prevProcessEndTime - serviceTime;
                waitLength = waitLength - arrivalTime;
                current_process[i].waitTime = waitLength; //store how long that process will have to wait until it starts
                finishTImecur = prevProcessEndTime;
            }
        }  
    }
      //following loop draws each bar (one at a time) showing how the processes are executed
     for(int j=0;j<processCount;j++){ //for each process
       int current_servTime = current_process[j].serviceTime;
       char current_process_name[11];
       int entryTime = current_process[j].entryTime;
       printf("%s entered the system at %d seconds.</n>",current_process[j].process_name,entryTime);
       printf("%s is in running state</n>",current_process[j].process_name);
       int waitPeriod = current_process[j+1].waitTime;
       strcpy(current_process_name, current_process[j].process_name);
       appendRow(current_process_name);
       appendBlank(j,entryTime);
       if(waitPeriod!=0){
          for(int z = 0;z<waitPeriod;z++){ 
            sleep(1);
            appendBar(j,(z/z),"red","Wait period",1); //z/z to result in 1, which is what we append by
          }
       }
      for (int y = 0;y<current_servTime; y++){
         sleep(1);
         appendBar(j,(y/y),"green","executing",0);
      }
       printf("%s completed. Turnaround time %d seconds. Total wait time: %d</n>",current_process[j].process_name,current_servTime,waitPeriod);
        
    } 
        waitExit();
        fclose(fptr);

    printf("\n");
}