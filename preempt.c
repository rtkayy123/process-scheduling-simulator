/*


    Program illustrating a Round Robin Scheduling algorithm visually.
    Started on Fri. 11th of October 2019.
    Last Modified on Sun. 13th of October 2019.
    

    The program reads a file passed as an argument - this file should have 3 columns, processName, arrival time, service time;
    Program reads these values and makes a visual representation of their execution and waiting states if Round Robin Scheduling is applied 
    with a time slice of 2 seconds;

*/


#include "process-visualiser.h"
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include  <unistd.h>
#define LSIZ 128  
void passFile(char *arr);

int count = 0;
int i=0;

 /* Special enumerated data type for process state */
typedef enum {
     READY, RUNNING, EXIT
 } process_state_t;
/*C data structure used as process control block. The scheduler should create
one instance per running process in the system  */
typedef struct {
  char process_name[11]; //a string that defines the process
  /* Times measured in seconds...*/
  int entryTime;    //time process entered system
  int serviceTime;  //total CPU time required by the process
  int remainingTime;  //remaining service time until completion
  

  process_state_t state; //current process state (e.g READY)
 } pcb_t;
 
void appendMyRows(pcb_t myProcesses[],int rowIndex[],int processCount); //appends the Rows on the chart with the data from file; Method takes in processNames,Index,and how many processes there are
void rrScheduler(pcb_t myProcesses[], int rowIndex[], int processCount, int system); //implements the Round Robin Scheduling and draws the chart
int main(int argc, char **argv){
  initInterface("silver","blue");
  if(argc <2){
    passFile("process-data.txt");
  }
  if(argc == 2){
    passFile(argv[1]);
  } else {
      printf("Invalid arguments");
      exit(0);
  }
  return 0;
}

void passFile(char *arr){
   char line[LSIZ];
   char str[11];
   char str1[11];
   int arrivalTime;
   int serviceTime;
   int prevProcessEndTime = 0;
   int waitingTime = 0;
   int i = 0;
   FILE *fptr = NULL;
   int processCount = -1;
   int finishTime;
   int finishTImecur =0;
   int waitLength = 0;
   fptr = fopen(arr,"r");
   char c;
   for(c = getc(fptr);c!=EOF;c=getc(fptr)){
    if(c=='\n'){
      processCount = processCount+1;
    }
   }
   pcb_t current_process[processCount];
   int rowIndex[10]; //max size of processes
   if ( NULL != ( fptr = fopen(arr, "r"))) {
        while ( fgets ( line, LSIZ, fptr)){
            if ( 3 == sscanf ( line, "%10s %d %d", str1, &arrivalTime, &serviceTime)) { //Store each column and row value as a single item
                process_state_t current_state = READY;
                strcpy(current_process[i].process_name, str1);
                current_process[i].entryTime = arrivalTime;
                current_process[i].serviceTime = serviceTime;
                current_process[i].remainingTime = waitingTime+serviceTime;
                current_process[i].state = READY;
                i = i+1;     
            }
        }  
    }
    pcb_t buffrd; //current process we're going to work with to calculate time indices 
    for(int index = 0;index<processCount;index++){
      for(int index2 = 0;index2<processCount-1;index2++){
          if(current_process[index2].entryTime>current_process[index2+1].entryTime){ //exchange the processes
            buffrd = current_process[index2];
            current_process[index2] = current_process[index2+1];
            current_process[index2+1] = buffrd;
          }
      }
    }
    appendMyRows(current_process,rowIndex,processCount);  //create the rows

   
    waitExit();
    fclose(fptr);

    printf("\n");
}
void appendMyRows(pcb_t myProcesses[],int rowIndex[], int processCount){
    int i = 0;
    int quantum = 0;
    for (int i = 0; i < processCount; i++){               //create the rows
        rowIndex[i] = appendRow(myProcesses[i].process_name);      /
        appendBlank(rowIndex[i], myProcesses[i].entryTime);
    }        //create blankspace
    rrScheduler(myProcesses,rowIndex,processCount,quantum);
}
void rrScheduler(pcb_t myProcesses[], int rowIndex[], int processCount, int entry){
  for (int i=0;i<processCount;i++){
    if(myProcesses[i].entryTime<=entry && myProcesses[i].remainingTime!=0){ //if entryTime is not current timeline, process is waiting
      for(int a = myProcesses[i].entryTime; a<=entry;a++){
        appendBar(rowIndex[i],1,"red","wait period",1);
      }
      int waitingTime = entry - myProcesses[i].entryTime; //get the actual number of how long its waiting
      myProcesses[i].state==READY;
    }
    if(myProcesses[i].remainingTime==0){
      myProcesses[i].state==EXIT; //if there's no more remaining time, process is in exit state
    }
    if(myProcesses[i].remainingTime>0){ //if not, append the bar by 2 (for loop) and state is running
      for(int z = 0;z<2;z++){ //2 is the time slice
        if (myProcesses[i].remainingTime == 0) {
          myProcesses[i].state == EXIT;
          break;
        }
        appendBar(rowIndex[i], 1, "green", "executing", 0);
        sleep(1);
        entry++;
        myProcesses[i].state = RUNNING;
        myProcesses[i].remainingTime--;
        myProcesses[i].entryTime = entry;
      }
    }
    if (myProcesses[i].state == EXIT) { //if process has left CPU, then increment number of completed processes
      int i = 0;
      count++;
    }
    int tat = entry - myProcesses[i].entryTime;
    if(i!=0){
      printf("Process %s has entered at %d  seconds</n>", myProcesses[i].process_name, myProcesses[i].entryTime);
    }
    printf("Process %s has Turnaround Time of %d  seconds</n>", myProcesses[i].process_name, tat);
    printf("Process %s has exited the System at %d  seconds</n>", myProcesses[i].process_name, entry);
    myProcesses[i].state=EXIT;
    if(count==4){ //if completed processes = 4 then exit loop
      break;
    }
  }
  printf("Duration %d</n>",entry);
  entry--;
  i++;
  if(count!=4){ //if completed processes is not 4, enter loop
    rrScheduler(myProcesses,rowIndex,processCount,entry);
  }

}